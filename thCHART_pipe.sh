#!/bin/bash
# Pipeline for creating fold enrichemnt tracks from thCHART genomic sequencing data 
# Published in Machyna et al. 2019
# 
# Folder must contain sequencing data in format NAME_R1.fastq NAME_R2.fastq
# User must specify which sample number is input (when sample names are ordered alphabetically)
# Following software must be installed an accesible form PATH: 
#	Cutadapt >1.7, Bowtie 2.2.9, MACS 2.1.0, SAMtools 1.4, BEDTools 2.26.0
#	bedGraphToBigWig (http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/bedGraphToBigWig)
# 
#
# Usage: thCHART_pipe.sh <input number>
# e.g. thCHART_pipe.sh 1


# Set up parameters
cpu=20			# Number of parallel cores <INT>
reads="PE"		# Paird-end or single-end reads <PE|SE> 
genome="dm6"	# Genome version to use <dm6|mm10|hg38>


# Correct input file number for 0-base array
input=$(($1-1))

# Scan for data
data=( $(ls *.fastq | awk -F '_R' '{print $1}' | uniq) )


# Make .bam alignment file for each dataset
for name in ${data[@]}
do
	# Trim Illumina adaptor sequences
	# Align wiht Bowtie 2
	if [ $reads = "PE"]
	then
		cutadapt \
				-a AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC \
			    -A AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT \
			    -m 3 \
			    -o "$name"_R1.t.fastq -p "$name"_R2.t.fastq \
			    "$name"_R1.fastq "$name"_R2.fastq

		bowtie2 -p "$cpu" \
		        -x ~/software/genomes/bowtie2/${genome} \
		        -1 "$name"_R1.t.fastq \
		        -2 "$name"_R2.t.fastq \
		        -S "$name".sam
	else
		cutadapt \
				-a AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC \
				-m 3 \
			    -o "$name"_R1.t.fastq \
			    "$name"_R1.fastq

		bowtie2 -p "$cpu" \
		        -x ~/software/genomes/bowtie2/${genome} \
		        -U "$name".t.fastq \
		        -S "$name".sam
	fi   
	    
	
	
	# convert SAM to BAM
	samtools view -b -q 2 -@ "$cpu" \
		-o "$name".bam "$name".sam
	    
	# sort BAM
	samtools sort -@ "$cpu" "$name".bam -o "$name"_sort.bam
	
	# remove .sam and old .bam
	rm "$name".sam
	rm "$name".bam
	
	# rename sorted bam
	mv "$name"_sort.bam "$name".bam
	
	# remove temp fastq files
	rm "$name"_R{1,2}.t.fastq 

	
done

# Make BigWig enrichment tracks with MACS2
for name in ${data[@]:0:$input} ${data[@]:$(($input+1))}
do

	if [ $reads = "PE"]
	then
		fparam="BAMPE"
	else
		fparam="BAM"
	fi

	# Call peaks with MACS2
	macs2 callpeak \
		-t "$name".bam \
		-c "${data[$input]}".bam \
		-n "$name" -f "$fparam" -g ${genome:0:2} -B


	# Create fold enrichment track
	macs2 bdgcmp \
		-t "$name"_treat_pileup.bdg \
		-c "$name"_control_lambda.bdg \
		-o "$name"_FE.bdg -m FE

	# Sort BedGraph files uppercase letter before lowercase
	LC_COLLATE=C sort -k1,1 -k2,2n "$name"_FE.bdg > "$name"_FE_sort.bdg
	LC_COLLATE=C sort -k1,1 -k2,2n "$name"_treat_pileup.bdg > "$name"_treat_pileup_sort.bdg

	# Convert to bigWig files
	# Requires file with chromosome sizes
	bedGraphToBigWig \
		"$name"_FE_sort.bdg \
		~/software/bedtools/${genome}.chrom.sizes \
		"$name"_FE.bigWig
		
	bedGraphToBigWig \
		"$name"_treat_pileup_sort.bdg \
		~/software/bedtools/${genome}.chrom.sizes \
		"$name"_pileup.bigWig
	
	# Remove bedgraphs
	rm "$name"_FE.bdg
	rm "$name"_FE_sort.bdg
	rm "$name"_treat_pileup.bdg
	rm "$name"_treat_pileup_sort.bdg
	

done

# Make Input BigWig
	# Sort BedGraph files uppercase letter before lowercase
	LC_COLLATE=C sort -k1,1 -k2,2n "$name"_control_lambda.bdg > "$name"_control_lambda_sort.bdg

	# Convert to bigWig
	bedGraphToBigWig \
		"$name"_control_lambda_sort.bdg \
		/home/mm2594/software/bedtools/${genome}.chrom.sizes \
		"${data[$input]}"_input.bigWig
	
	# Remove bedgraphs
	rm *_control_lambda.bdg
	rm "$name"_control_lambda_sort.bdg

